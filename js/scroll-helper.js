/**
 * Scrollr control functions
 */
(function ($) {
	$(function () {
    var $body = $('body'),
      keys = [37, 38, 39, 40, 32, 33, 34, 35, 36],
      scrollTimeout;

    window.scrollHelper = {};


    function preventDefault(evt) {
      evt = evt || window.event;
      if (evt.preventDefault) {
        evt.preventDefault();
      }
      evt.returnValue = false;
    }

    function keydown(evt) {
      for (var idx = keys.length; idx--;) {
        if (evt.keyCode === keys[idx]) {
          preventDefault(evt);
          return;
        }
      }
    }

    function wheel(evt) {
      preventDefault(evt);
    }

    /**
     * Pause scroll on any position
     * @param position integer
     */
    window.scrollHelper.pause = function(position) {
      window.scrollHelper.unpause();

      $body.addClass('scrollr-paused').data('scrollr-position', position);

      if (window.addEventListener) {
        window.addEventListener('DOMMouseScroll', wheel, false);
      }
      window.onmousewheel = document.onmousewheel = wheel;
      document.onkeydown = keydown;

      $(window).on('scroll.pause', function () {
        clearTimeout(scrollTimeout);
        scrollTimeout = setTimeout(function () {
          $body.scrollTop(position);
        }, 10);
      });
    };

    /**
     * Play scroll
     */
    window.scrollHelper.unpause = function() {
      clearTimeout(scrollTimeout);
      $body.removeClass('scrollr-paused').data('scrollr-position', false);

      if (window.removeEventListener) {
        window.removeEventListener('DOMMouseScroll', wheel, false);
      }
      window.onmousewheel = document.onmousewheel = document.onkeydown = null;

      $(window).off('scroll.pause');
    };

    /**
     * Check if scroll is paused
     * @returns boolean
     */
    window.scrollHelper.isPaused = function() {
      return $body.hasClass('scrollr-paused');
    };

    /**
     * Get position for paused scroll
     * @returns int
     */
    window.scrollHelper.getPausePosition = function() {
      return parseInt($body.data('scrollr-position'), 10);
    };

    /**
     * Check if scrollr can play
     * @returns {boolean}
     */
    window.scrollHelper.check = function() {
      if (window.scrollHelper.isPaused()) {
        var requiredScrollPosition = window.scrollHelper.getPausePosition();

        if (requiredScrollPosition !== $body.scrollTop()) {
          $body.scrollTop(requiredScrollPosition);
        }

        //return false;
      }
    };

    /**
     * Scrollr animate
     * @param identifier string for debugging only
     * @param scrollrInstance scrollr object
     * @param position integer
     * @param onReady function
     * @param duration integer
     */
    window.scrollHelper.animateTo = function (identifier, scrollrInstance, position, onReady, duration) {
      onReady = onReady || function() {};
      duration = parseInt(duration, 10) || 1000;

      // for debugging:
      // console.log(identifier + ': ' + position);

      scrollrInstance.animateTo(position, {
        duration: duration,
        easing:   'swing',
        done:     onReady
      });
    };

    /**
     * Unlock stage on progress-bar. Unlock number-stage & all previous stages
     * @param number integer
     */
    window.scrollHelper.unlockStage = function (number) {
      do {
        $('.js-stage[data-stage="' + number + '"]').addClass('unlocked');
      } while (--number > 0);
    };

    /**
     * Calculate scroll position by obj's data attributes data-anchor & data-shift
     *   data-anchor — scrollr constant
     *   data-shift — related to data-anchor shift
     * @param obj DOMObject | JQueryObject
     * @returns {*}
     */
    window.scrollHelper.getScrollPositionByData = function (obj) {
      var $obj = $(obj),
        anchorName = $obj.data('anchor'),
        anchorShift = parseInt($obj.data('shift'), 10),
        anchorPosition = 0;

      anchorShift = (isNaN(anchorShift) ? 0 : anchorShift);
      if (anchorName && APP.scrollConstants.hasOwnProperty(anchorName)) {
        anchorPosition = APP.scrollConstants[anchorName];
      }

      return anchorShift + anchorPosition + 1;
    };
	});
}(jQuery));
