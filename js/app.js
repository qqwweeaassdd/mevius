(function ($) {
	'use strict';

	// Avoid `console` errors in browsers that lack a console.
	var method,
		noop = function () {},
		methods = [
			'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error', 'exception', 'group', 'groupCollapsed',
			'groupEnd', 'info', 'log', 'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd', 'timeStamp',
			'trace', 'warn'
		],
		length = methods.length,
		console = (window.console = window.console || {});

	while (length--) {
		method = methods[length];
		if (!console[method]) { // Only stub undefined methods.
			console[method] = noop;
		}
	}

	/**
	 * On ready
	 */
	$(function () {
    var $body = $('body'),
      $game = $('.js-game'),

      slide02 = 5000,
      slide03 = slide02 + 12000,
      slide04 = slide03 + 12900,
      game01 = slide04 + 11200,
      slide05 = game01 + 2400,
      game02 = slide05 + 9200,
      slide06 = game02 + 1000,
      game03 = slide06 + 10000,
      slide07 = game03 + 0,
      game04 = slide07 + 8500,
      slide08 = game04 + 2800,

      lss1 = slide04 + 3700,
      lss2 = slide04 + 9000,
      cns1 = slide05 + 2300,
      cns2 = slide05 + 6500,
      r361 = slide06 + 4000,
      r362 = slide06 + 6600,

      LSSMaskStart = slide04,
      LSSMaskEnd = slide04 + 2500,
      $animateImages = $('.js-animate-image'),

      baseImgDir = 'img/',
      loaderSources = [
        'Mevius_preload_propeller.png',
        'bg/BG_01.jpg',

        'waves/mev_waves_L1.png',
        'waves/mev_waves_L2.png',
        'waves/mev_waves_L3.png',
        'waves/mev_waves_L4.png',
        'mev_wave_transition_01.png',
        'MW_logo.png',
        's.gif',
        'sunbeam_01.png',
        'quest_button.svg',
        '360/360_illustration_01.png',
        '360/360_illustration_02.png',
        '360/360_logo_big.png',
        '360/info_ico2.png',
        'bg/BG_02.jpg',
        'bg/BG_03.jpg',
        'bg/BG_04.jpg',
        'game/answer-mask.png',
        'game/blue/answer-1.png',
        'game/blue/answer-2.png',
        'game/blue/answer-3.png',
        'game/blue/logo.svg',
        'game/blue/question.png',
        'game/color/270.jpg',
        'game/color/280.jpg',
        'game/color/340.jpg',
        'game/color/logo.svg',
        'game/color/question.jpg',
        'game/dark-blue/answer-1.jpg',
        'game/dark-blue/answer-2.jpg',
        'game/dark-blue/answer-3.jpg',
        'game/dark-blue/logo.svg',
        'game/dark-blue/question.jpg',
        'game/violet/answer-1.jpg',
        'game/violet/answer-2.jpg',
        'game/violet/answer-3.jpg',
        'game/violet/logo.svg',
        'game/violet/question.jpg',
        'LSS/dots_BG_01.png',
        'LSS/LSS_about.png',
        'LSS/LSS_icon_01.png',
        'LSS/LSS_icon_02.png',
        'LSS/LSS_icon_03.png',
        'LSS/LSS_icon_04.png',
        'LSS/LSS_icon_05.png',
        'LSS/LSS_illustration.png',
        'LSS/LSS_logo_big.png',
        'packs/SKU_Original_01.jpg',
        'packs/SKU_SkyBlue_01.jpg',
        'packs/SKU_WindBlue_01.jpg',
        'panel-right/progressbar-body.jpg',
        'panel-right/SB_blue-tip.svg',
        'panel-right/SB_icon_disk.svg',
        'panel-right/SB_icon_lock.svg',
        'panel-right/SB_icon_ring.svg',
        'panel-right/SB_icon_unlock.svg',
        'panel-right/SB_white-tip.svg',
        'panel-right/SB_yellow-tip.svg',
        'popup/popup_body.png',
        'popup/popup_visual_anim_sprt_w921h436.png',
        'progress-bar/Mevius_small_propeller.png',
        'progress-bar/SB_icon_disk.svg',
        'progress-bar/SB_icon_lock.svg',
        'progress-bar/SB_icon_ring.svg',
        'progress-bar/SB_icon_unlock.svg',
        'slide-01/light_01_x2reduced.png',
        'slide-01/light_02_x2reduced.png',
        'slide-01/scroll_icon.gif',
        'slide-03/360_logo.svg',
        'slide-03/CnS_logo.svg',
        'slide-03/light_03.png',
        'slide-03/LSS_logo.svg',
        'slide-03/mevius_logo_big.png',
        'slide-03/mevius_logo_text.png',
        'slide-03/SKU_SkyBlue_open.png',
        'slide-05/closenseal_logo_big.png',
        'slide-05/CnS_icon_01.png',
        'slide-05/CnS_pack.png',
        'slide-05/CnS_pack_action.png',
        'slide-05/CnS_pack_glow.png',
        'slide-05/mev_wave_transition_02.png',
        'slide-05/ORB_reduced_bottom.png',
        'slide-05/ORB_reduced_top.png',
        'slide-07/main-img.png',
        'slide-07/SKU_SkyBlue_original_02.png',
        'slide-07/SKU_SkyBlue_skyblue_02.png',
        'slide-07/SKU_SkyBlue_windblue_02.png',
        'slide-08/logo_360.png',
        'slide-08/logo_cs.png',
        'slide-08/logo_lss.png',
        'slide-08/mevius_logo_big.png',
        'slide-08/SKU_SkyBlue_sprt_w304h505.png',
        'svg/rules_prize.svg',
        'svg/rules_quest.svg',
        'svg/rules_read.svg',
        'svg/rules_unlock.svg'
      ];

    APP.scrollConstants = {
      slide02: slide02,
      slide03: slide03,
      slide04: slide04,
      slide05: slide05,
      slide06: slide06,
      slide07: slide07,
      slide08: slide08,

      game01: game01,
      game02: game02,
      game03: game03,
      game04: game04,

      lss1: lss1,
      lss2: lss2,
      cns1: cns1,
      cns2: cns2,
      r361: r361,
      r362: r362,

      LSSMaskStart: LSSMaskStart,
      LSSMaskEnd: LSSMaskEnd
    };


    /**
     * Loader
     * @param onReady callback
     */
    function loader(onReady) {
      var loadedCnt = 0;
      $.each(loaderSources, function (key, item) {
        $('<img>').on('load error', function () {
          if (++loadedCnt >= loaderSources.length && typeof onReady === 'function' && typeof APP.curLevel != 'undefined') {
            onReady();
          }
        }).attr('src', baseImgDir + item);
      });

      // get cur level
      $.ajax({
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        url: 'game.aspx/GetSave',
        success: function (data) {
          APP.curLevel = parseInt(data.d.toString(), 10);
          APP.curLevel = isNaN(APP.curLevel) ? 0 : APP.curLevel;

          if (loadedCnt >= loaderSources.length && typeof onReady === 'function') {
            onReady();
          }
        },
        error: function () {
          APP.curLevel = 0;

          if (loadedCnt >= loaderSources.length && typeof onReady === 'function') {
            onReady();
          }
        }
      });
	  }

    /**
     * Main
     */
    loader(function () {
//APP.curLevel = 4; //
      APP.scroll = skrollr.init({
        skrollrBody: 'ch',
        constants: APP.scrollConstants,
        render: renderScrollr
      });

      function renderScrollr(data) {
        var scroll = this,
          $html = $('html'),
          hasSvgAnimation = ($html.hasClass('svg') && $html.hasClass('smil') && $html.hasClass('svgclippaths'));

        // svg-icons animation
        $animateImages.each(function () {
          var $this = $(this),
            isVisible = (APP.Helpers.between(data.curTop, slide02 + $this.data('start'), slide02 + $this.data('end')));

          if ($this.data('is-visible') !== isVisible) {
            if (isVisible) {
              var srcAttr = hasSvgAnimation ? 'src' : 'src-static';
              $this.find('img').prop('src', $this.data(srcAttr) + '?' + (new Date()).getTime());
            }

            $this.data('is-visible', isVisible);
          }
        });

        if (this.isAnimatingTo()) {
          return;
        }


        // init questions
        if (typeof APP !== 'undefined' && typeof APP.initQuestion !== 'undefined') {
          if (APP.Helpers.between(data.curTop, game01 - 2500, slide05 - 1)) {
            APP.initQuestion(0);
          }
          if (APP.Helpers.between(data.curTop, game02 - 2500, slide06 + 2000)) {
            APP.initQuestion(1);
          }
          if (APP.Helpers.between(data.curTop, game03 - 3500, slide07 + 2000)) {
            APP.initQuestion(1);
            APP.initQuestion(2);
          }
          if (APP.Helpers.between(data.curTop, game04, game04 + 3500)) {
            APP.initQuestion(3);
          }
        }


        // autoplays
        if (APP.Helpers.between(data.curTop, slide04 + 5900, slide04 + 9000) && data.direction === 'down') {
          window.scrollHelper.animateTo('Autoplay LSS: ', scroll, lss2 + 10, null, 2000);
        }


        /**
         * games animation
         * @param gameIdx integer
         * @returns {boolean}
         */
        function gameShow(gameIdx) {
          var pausePosition,
            games = [
              {
                isLocked: ($game.data('complete-') != 1),
                top: game01 - 999,
                bottom: slide05,
                questionState: game01 - 201,
                questionStateBottom: slide05 - 2000,
                activeState: game01 + 1
              },
              {
                isLocked: ($game.data('complete-violet') != 1),
                top: game02 - 999,
                bottom: slide06 - 500,
                questionState: game02 - 201,
                questionStateBottom: slide06 - 250,
                activeState: game02 + 1
              },
              {
                isLocked: ($game.data('complete-dark-blue') != 1),
                top: game03 - 2251,
                bottom: slide07 - 500,
                questionState: game03 - 1201,
                questionStateBottom: slide07 - 1000,
                activeState: game03 - 999
              }
            ],
            gameInfo = games[gameIdx];

          if (typeof gameInfo === 'undefined') {
            return true;
          }
//console.log((gameInfo.isLocked && data.curTop > gameInfo.top), APP.Helpers.between(data.curTop, gameInfo.questionState, gameInfo.questionStateBottom));
//console.log(gameIdx, gameInfo.isLocked, gameInfo.questionState + '<' + data.curTop + '<' + gameInfo.questionStateBottom);
          if ((gameInfo.isLocked && data.curTop > gameInfo.top)
            /*|| (APP.Helpers.between(data.curTop, gameInfo.top, gameInfo.bottom))*/) {

            if (APP.Helpers.between(data.curTop, gameInfo.questionState, gameInfo.questionStateBottom)) {
              pausePosition = data.curTop;
            } else {
              pausePosition = gameInfo.activeState;
            }

            window.scrollHelper.pause(pausePosition);
            window.scrollHelper.animateTo('Game show: ', scroll, pausePosition);

            return false;
          }

          return true;
        }

        /**
         * Show popup
         * @param $popup JQueryObject
         */
        function popupShow($popup) {
          var positionTop = window.scrollHelper.getScrollPositionByData($popup),
            positionBottom = positionTop + 50;

          // first show
          if ($popup.data('passed') !== true && data.curTop >= positionTop) {
            window.scrollHelper.pause(positionBottom);
            $body.scrollTop(positionBottom);
            window.scrollHelper.animateTo('Popup show1: ', scroll, positionBottom);
            return false;
          }

          // other shows
          /*if ($popup.data('show-once') !== 1 &&
           data.direction === 'down' &&
           APP.Helpers.between(data.curTop, positionTop, positionBottom)) {

           window.scrollHelper.pause(positionBottom);
           //$body.scrollTop(positionBottom);
           window.scrollHelper.animateTo('Popup show2: ', scroll, positionBottom, function () {
           window.scrollHelper.unpause();
           });
           }*/

           return true;
        }


        // popups
        var $stoppers = [
            $('#popup-welcome'),
            $('#popup-1-task'),
            $game,
            $('#popup-1-complete'),
            $('#popup-2-task'),
            $game,
            $('#popup-2-complete'),
            $('#popup-3-task'),
            $game,
            $('#popup-3-complete')
          ],
          gameIdx = 0;

        $.each($stoppers, function (key, item) {
          var $stopper = $(item);

          if ($stopper.hasClass('popup')) {
            return popupShow($stopper);
          }
          if ($stopper.hasClass('js-game')) {
            return gameShow(gameIdx++);
          }
        });

        // last game
        if ($game.data('complete-color') != 1 && data.curTop > game04) {
          //window.scrollHelper.pause(scroll, game04 + 1900);
          window.scrollHelper.animateTo('Render1: ', scroll, game04 + 1900);
        }


        // LSS mask resize
        if (APP.Helpers.between(data.curTop, LSSMaskStart, LSSMaskEnd)) {
          $(window).trigger('resize');
        }
      }



      var $preloader = $('.js-preloader');
      $preloader.fadeOut('fast', function () {
        $preloader.remove();
        $('.js-slides').fadeIn('fast');
      });


      // go to cur level
      window.scrollHelper.unlockStage(APP.curLevel + 1);

      switch (APP.curLevel) {
        // first passed
        case 1: {
          APP.questions[0].userAnswers = APP.questions[0].answers;

          $('#popup-welcome, #popup-1-task, #popup-1-complete')
            .data('passed', true);
          $game.data('complete-', 1);
          window.scrollHelper.animateTo('Switch level2: ', APP.scroll, cns1 + 10);
          break;
        }

        // first & second passed
        case 2: {
          APP.questions[0].userAnswers = APP.questions[0].answers;
          APP.questions[1].userAnswers = APP.questions[1].answers;

          $('#popup-welcome, #popup-1-task, #popup-1-complete, #popup-2-task, #popup-2-complete')
            .data('passed', true);
          $game.data('complete-', 1).data('complete-violet', 1);
          window.scrollHelper.animateTo('Switch level3: ', APP.scroll, r361 + 10);
          break;
        }

        // first & second & third passed
        case 3: {
          APP.questions[0].userAnswers = APP.questions[0].answers;
          APP.questions[1].userAnswers = APP.questions[1].answers;
          APP.questions[2].userAnswers = APP.questions[2].answers;

          $('#popup-welcome, #popup-1-task, #popup-1-complete, #popup-2-task, #popup-2-complete, #popup-3-task, #popup-3-complete')
            .data('passed', true);
          $game.data('complete-', 1).data('complete-violet', 1).data('complete-dark-blue', 1);
          window.scrollHelper.animateTo('Switch level4: ', APP.scroll, slide07 + 4501);
          break;
        }

        // first & second & third & forth passed
        case 4: {
          APP.questions[0].userAnswers = APP.questions[0].answers;
          APP.questions[1].userAnswers = APP.questions[1].answers;
          APP.questions[2].userAnswers = APP.questions[2].answers;
          APP.questions[3].userAnswers = APP.questions[3].answers;

          $('#popup-welcome, #popup-1-task, #popup-1-complete, #popup-2-task, #popup-2-complete, #popup-3-task, #popup-3-complete')
            .data('passed', true);
          $game.data('complete-', 1).data('complete-violet', 1).data('complete-dark-blue', 1).data('complete-color', 1);
          window.scrollHelper.animateTo('Switch done: ', APP.scroll, slide08 + 20000);
          break;
        }

        // none passed
        default: {
          window.scrollHelper.animateTo('Switch none: ', APP.scroll, 1200);
        }
      }
    });


    /**
     * Show complete popup
     * @param type string game type
     */
    APP.showCompletePopup = function (type) {
      var popupIdx = (type === '' ? 1 : (type === 'violet' ? 2 : (type === 'dark-blue' ? 3 : 4))),
        $popup = $('#popup-' + popupIdx + '-complete'),
        position = window.scrollHelper.getScrollPositionByData($popup) + 50;

      window.scrollHelper.unlockStage(popupIdx + 1);

      // send complete level
      $.ajax({
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        url: 'game.aspx/SetSave',
        data: '{\"save\":\"' + popupIdx + '\"}',
        success: function (data) {
          //alert(data.d.toString());
        }
      });


      if (popupIdx === 4) {
        setTimeout(function () {
          window.scrollHelper.unpause();
          window.scrollHelper.animateTo('Fake popup: ', APP.scroll, slide08 + 5200, null, 3000);
        }, 1000);

        // send complete game
        $.ajax({
          type: 'POST',
          dataType: 'json',
          contentType: 'application/json',
          url: 'game.aspx/Win',
          success: function (data) {
            if (!data.d) {
              alert('Ошибка, не получилось сохранить данные.');
            }
          },
          error: function () {
            alert('Ошибка, не получилось сохранить данные.');
          }
        });

        return;
      }

      $('#popup-' + popupIdx + '-task').hide();
      $('#popup-' + popupIdx + '-question').css('opacity', 0).hide();

      window.scrollHelper.pause(position);
      window.scrollHelper.animateTo('True popup: ', APP.scroll, position);
    };


    /**
     * Popup continue buttons
     */
    $('.js-popup-button').on('click', function () {
      var $obj = $(this);

      // scroll to position
      var position = window.scrollHelper.getScrollPositionByData($obj);

      window.scrollHelper.unpause();
      window.scrollHelper.animateTo('Popup button: ', APP.scroll, position, function () {
        var $popup = $obj.closest('.popup');
        if ($popup.data('show-once') === 1) {
          $popup.hide();
        }
      }, $obj.data('duration'));

      $obj.closest('.popup').data('passed', true);
    });
    /**
     * /Popup continue buttons
     */

    $('.js-question > div').on('click', function () {
      var position = window.scrollHelper.getScrollPositionByData(this);
      window.scrollHelper.animateTo('Question: ', APP.scroll, position);
    });


    // Navigate anchors
    $('.js-navigate').on('click', function () {
      var position = window.scrollHelper.getScrollPositionByData(this);
      window.scrollHelper.animateTo('Navigate: ', APP.scroll, position);
    });
  });
}(jQuery));
