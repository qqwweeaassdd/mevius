(function ($) {
  'use strict';

  $(function () {
    // LSS svg resize
    function lssSvgResize() {
      var ratio,
        svgRatio,
        marginHor,
        interval,
        bg1Width = 1645,
        bg1Height = 900,
        closingWaveWidth = 1286,
        closingWaveHeight = 1141,
        $lssSvg = $('#Layer_lss'),
        closingWave = $('#closingWave').get(0),
        wWidth = $(window).width(),
        wHeight = $(window).height();

      if (wWidth / wHeight >= bg1Width / bg1Height) {
        // regular case
        $lssSvg.get(0).setAttribute('preserveAspectRatio', 'xMidYMin slice');
        $lssSvg.css({
          margin: 0,
          width: '100%',
          height: '100%'
        });

        // closing wave
        /*
        closingWave.setAttribute('width', closingWaveWidth + 'px');
        closingWave.setAttribute('height', closingWaveHeight + 'px');
        closingWave.setAttribute('transform', 'translate(0 0)');
        */
      } else {
        // svg too short case
        //svgRatio = ((wWidth < 1108) ? 'xMidYMin' : 'xMidYMin slice'); // I don't know why 1108 :(
        svgRatio = 'xMidYMin slice';
        $lssSvg.get(0).setAttribute('preserveAspectRatio', svgRatio);
        ratio = 100 * (bg1Width / bg1Height) / (wWidth / wHeight);
        $lssSvg.css({
          width: ratio + '%',
          height: ratio + '%'
        });

        interval = setInterval(function () {
          // wait when width in percents is calculated to pixels
          if ($lssSvg.css('width').slice(-2) === 'px') {
            marginHor = ($('#Layer_lss').width() - wWidth) / 2;
            $lssSvg.css('margin', '0 -' + marginHor + 'px');
            clearInterval(interval);

            // closing wave
            /*
            var closingWaveRatio = wWidth / closingWaveWidth,
              closingWaveNewHeight;
            closingWaveRatio = (closingWaveRatio > 1 ? 1 : closingWaveRatio);
            closingWaveNewHeight = Math.ceil(closingWaveHeight * closingWaveRatio);

            closingWave.setAttribute('width', Math.ceil(closingWaveWidth * closingWaveRatio) + 'px');
            closingWave.setAttribute('height', closingWaveNewHeight + 'px');
            closingWave.setAttribute('transform', 'translate(0 ' + (closingWaveHeight - closingWaveNewHeight) + ')');
            */
          }
        }, 500);
      }
    }

    // Start watching LSS svg resize
    var resizeTimeout;
    setTimeout(function () {
      lssSvgResize();
      $(window).on('resize', function () {
        clearTimeout(resizeTimeout);
        resizeTimeout = setTimeout(function () {
          lssSvgResize();
        }, 50);
      });
    }, 1000);
  });

  $(function () {
    var $window = $(window);

    $window.on('mousemove', function (evt) {
      // @todo: Optimize. Calculate only when blocks are visible
      var windowWidth = $window.width(),
        windowHeight = $window.height();

      // Slide5. Background parallax
      var rate = (evt.pageX / windowWidth - .5);
      $('.js-slide5-bg1').css('margin-left', (-rate * 100) + 'px');
      $('.js-slide5-bg2').css('margin-left', (rate * 100 * 2) + 'px');
      $('.js-slide5-action')
        .css('margin-left', (rate * 90) + 'px')
        .css('margin-top', (rate * 15) + 'px');


      // Game2. lightbeam background parallax
      var pageY = evt.pageY - $window.scrollTop(),
        beamLeft = 1 + (evt.pageX / windowWidth - .5) * .08,
        beamTop  = 1 + (pageY / windowHeight - .5) * .05;

      $('.js-light-bg-parallax')
        .css('backgroundPosition', (beamLeft * 100) + '% ' + (beamTop * 100) + '%');
    });

    // Slide5. Border radius
    function changeSlide5Radius() {
      var wWidth = $window.width(),
        verticalRadius = wWidth / 5;

      $('.js-game2-bg').css('borderRadius', '0 100% 0 0 / 0 ' + verticalRadius + 'px 0 0');
      $('.js-game2-wave').css('top', (-5 - wWidth * 380 / 1000) + 'px');
    }

    // Slide6. Border radius
    function changeSlide6Radius() {
      var wWidth = $(window).width(),
        verticalRadius = wWidth / 2;

      if (APP.scrollConstants && $window.scrollTop() > APP.scrollConstants.game03 - 3000) {
        $('.js-game3-bg').css('borderRadius', '0 0 ' + wWidth + 'px 0 / 0 0 ' + verticalRadius + 'px 0');
      } else {
        $('.js-game3-bg').css('borderRadius', '0');
      }

      $('.js-game3-wave').css('margin-bottom', (10 - wWidth * 140 / 1000) + 'px');
    }

    // Slide7. Border radius
    function changeSlide7Radius() {
      var wWidth = $(window).width(),
        verticalRadius = wWidth / 2;

      if (APP.scrollConstants && $window.scrollTop() > APP.scrollConstants.game04 - 3000) {
        $('.js-game4-bg').css('borderRadius', '0 0 ' + wWidth + 'px 0 / 0 0 ' + verticalRadius + 'px 0');
      } else {
        $('.js-game4-bg').css('borderRadius', '0');
      }

      $('.js-game4-wave').css('margin-bottom', (10 - wWidth * 140 / 1000) + 'px');
    }

    var resizeTimeout;
    setTimeout(function () {
      changeSlide5Radius();
      changeSlide6Radius();
      changeSlide7Radius();
      $window.on('resize scroll', function () {
        clearTimeout(resizeTimeout);
        resizeTimeout = setTimeout(function () {
          changeSlide5Radius();
          changeSlide6Radius();
          changeSlide7Radius();
        }, 50);
      });
    }, 1000);
  });
}(jQuery));
