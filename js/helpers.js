(function ($) {
  'use strict';

  /**
   * Helpers section
   */
  APP.Helpers = $.extend(APP.Helpers, {
    /**
     * Check empty value
     * @param val
     * @returns {boolean}
     */
    empty: function (val) {
      return (typeof val === 'undefined' || String(val) === '' || Number(val) === 0 || val === null);
    },

    /**
     * Value to integer.
     * Translates everything to integer value. If it can be translated, we use parseInt(val, 10) function.
     * In other cases it will be zero (0). Don't work with variables,
     * created using new Number(), new String(), new Boolean().
     *
     * test / examples:
     * console.log('undefined= \t' + toInt()         ); // 0
     * console.log('NaN=       \t' + toInt(NaN)      ); // 0
     * console.log('null=      \t' + toInt(null)     ); // 0
     * console.log('true=      \t' + toInt(true)     ); // 1
     * console.log('false=     \t' + toInt(false)    ); // 0
     * console.log('\'\'=      \t' + toInt('')       ); // 0
     * console.log('\'df\'=    \t' + toInt('df')     ); // 0
     * console.log('\'3df\'=   \t' + toInt('3df')    ); // 3
     * console.log('\' 4 \'=   \t' + toInt(' 4 ')    ); // 4
     * console.log('\'9\'=     \t' + toInt('9')      ); // 9
     * console.log('0=         \t' + toInt(0)        ); // 0
     * console.log('2=         \t' + toInt(2)        ); // 2
     * console.log('2e+5=      \t' + toInt(2e+5)     ); // 200000
     * console.log('[]=        \t' + toInt([])       ); // 0
     * console.log('[4]=       \t' + toInt([4])      ); // 0
     * console.log('[\'4\']=   \t' + toInt(['4'])    ); // 0
     * console.log('[\'er\']=  \t' + toInt(['er'])   ); // 0
     * console.log('[4,6]=     \t' + toInt([4,6])    ); // 0
     * console.log('{a:4}=     \t' + toInt({a:4})    ); // 0
     * console.log('{a:4,b:5}= \t' + toInt({a:4,b:5})); // 0
     *
     * @param val mixed
     * @returns {number}
     */
    toInt: function (val) {
      if (!val || val instanceof Array) {
        return 0;
      }

      val = (val === true ? 1 : val);
      var intVal = parseInt('' + val, 10);

      return (isNaN(intVal) ? 0 : intVal);
    },

    /**
     * Max for array items
     * @param arr array
     * @returns {number}
     */
    arrayMax: function (arr) {
      return Math.max.apply(0, arr);
    },

    /**
     *
     * @param val integer
     * @param min integer
     * @param max integer
     * @returns {boolean}
     */
    between: function (val, min, max) {
      return (val >= min && val <= max);
    }
  });
}(jQuery));
