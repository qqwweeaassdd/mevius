(function ($) {
	'use strict';

	$(function () {
		var questionId,
      $game = $('.js-game');

    APP.questionsRus = {
      0: {
        type: '',
        variants: {
          1: 'Инновационная технология JTI',
          2: 'Меньше запаха дыма',
          3: 'Ментоловая капсула',
          4: 'Двуслойная сигаретная бумага',
          5: 'Изменение цвета дыма'
        },
        answers:  [1, 2, 4],
        userAnswers:  []
      },
      1: {
        type: 'violet',
        variants: {
          1: 'Сохраняет свежесть сигарет',
          2: 'Заменяет сигаретную фольгу',
          3: 'Удобно закрывать и фиксировать пачку',
          4: 'Открывает пачку сбоку',
          5: 'Позволяет дольше сохранить вкус'
        },
        answers:  [1, 3, 5],
        userAnswers:  []
      },
      2: {
        type: 'dark-blue',
        variants: {
          1: '360 угольных элементов',
          2: 'Придаёт объемный и приятный вкус',
          3: 'Обволакиваю щая волна вкуса',
          4: '3 степени фильтрации дыма',
          5: 'Круговая перфорация на основании фильтра'
        },
        answers:  [2, 3, 5],
        userAnswers:  []
      },
      3: {
        type: 'color',
        variants: {
          1: 'ЦЕНА<br>260т',
          2: 'ЦЕНА<br>280т',
          3: 'ЦЕНА<br>270т',
          4: 'ЦЕНА<br>340т',
          5: 'ЦЕНА<br>300т',
          6: 'ЦЕНА<br>290т'
        },
        answers:  [4, 5, 6],
        userAnswers:  []
      }
    };

    APP.questionsKaz = {
      0: {
        type: '',
        variants: {
          1: 'Инновациялық технология',
          2: 'Түтін иісі азырақ',
          3: 'Ментол капсуласы',
          4: 'Екіқабат темекi қағазы',
          5: 'Түтіннің түсі өзгереді'
        },
        answers:  [1, 2, 4],
        userAnswers:  []
      },
      1: {
        type: 'violet',
        variants: {
          1: 'Темекінің алғашқы сапасын сақтайды',
          2: 'Қаптама ішіндегі темекілердің арасын бөліп тұрады',
          3: 'Қаптаманы жабуға және бекітуге ыңғайлы',
          4: 'Қаптаманы бүйірінен ашады',
          5: 'Дәмін ұзағырақ сақтау мүмкіндігін береді'
        },
        answers:  [1, 3, 5],
        userAnswers:  []
      },
    2: {
        type: 'dark-blue',
        variants: {
          1: '360 көмір элементтері',
          2: 'Көлемді және жағымды дәм береді',
          3: 'Баурап алатын дәм толқыны',
          4: 'Түтінді 3 дәрежелі сүзу',
          5: 'Сүзгі ұшындағы айналма тесу'
        },
        answers:  [2, 3, 5],
        userAnswers:  []
      },
      3: {
        type: 'color',
        variants: {
          1: 'бағасы<br>260тг',
          2: 'бағасы<br>280тг',
          3: 'бағасы<br>270тг',
          4: 'бағасы<br>340тг',
          5: 'бағасы<br>300тг',
          6: 'бағасы<br>290тг'
        },
        answers:  [4, 5, 6],
        userAnswers:  []
      }
    };

    APP.questions = $('body').hasClass('kaz') ? APP.questionsKaz : APP.questionsRus;

    $('.js-draggable').draggable();
		$('.js-droppable').droppable({
			accept: '.js-draggable',
			tolerance: 'intersect',
			drop: function(evt, ui) {
				var answerValue = parseInt(ui.draggable.data('value'), 10),
					question = APP.questions[questionId];

				for (var idx = 0; idx < question.answers.length; idx++) {
					if (question.answers[idx] == answerValue) {
						// answer correct
						if (typeof APP.questions[questionId].userAnswers === 'undefined') {
							APP.questions[questionId].userAnswers = [];
						}

						APP.questions[questionId].userAnswers.push(answerValue);
						var answerCnt = APP.questions[questionId].userAnswers.length;
						ui.draggable.addClass('selected');
						$('.js-question-answers').attr('data-state', answerCnt);
						initText(answerCnt, question.variants[answerValue]);

            // check complete
            var type = $game.attr('data-type');
            if (checkComplete(question)) {
              $game.data('complete-' + type, 1);
              APP.showCompletePopup(type);
            }

						return;
					}
				}

				// wrong answer
				$(this).removeClass('over');
				ui.draggable.addClass('fail');
				setTimeout(function () {
					ui.draggable.removeClass('fail');
				}, 1500);
			},
			deactivate: function(evt, ui) {
				$(this).removeClass('over');
				ui.draggable.css({
					left: '',
					top: ''
				});
			},
			over: function (evt, ui) {
				$(this).addClass('over');
			},
			out: function (evt, ui) {
				$(this).removeClass('over');
			}
		});

    /**
     * Check answers
     * @param question object question info
     * @returns {boolean}
     */
		function checkComplete(question) {
		  for (var idx = 0; idx < question.answers.length; idx++) {
		    var answer = parseInt(question.answers[idx], 10);
		    if ($.inArray(answer, question.userAnswers) === -1) {
          return false;
		    }
		  }

		  return true;
		}


		/**
		 * Init round text
		 * @param idx integer (1 | 2 | 3)
		 * @param text string
		 */
		function initText(idx, text) {
			if (idx == 1) {
				$('.js-answer-1 b').html(text).elipText({ radius: 135 });
			}
			if (idx == 2) {
				$('.js-answer-2 b').html(text).elipText({ radius: 170 });
			}
			if (idx == 3) {
				$('.js-answer-3 b').html(text).elipText({ radius: 210 });
			}
		}

		//+ Jonas Raoni Soares Silva
		//@ http://jsfromhell.com/array/shuffle [v1.0]
		function shuffle(o) { //v1.0
			for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
			return o;
		}

		/**
		 * Init scene for question
		 */
    APP.initQuestion = function(number) {
			if (typeof number === 'undefined') {
        number = 0;
			}
      questionId = number;

      if ($game.attr('data-type') == APP.questions[questionId].type) {
        return;
      }

			var isSelected,
			  $answer = $('.js-draggable').first(),
				behaviorArrValues = [1, 2, 3, 4, 5, 6],
				behaviorArr = shuffle(behaviorArrValues.slice());

			$.each(APP.questions[questionId].variants, function (key, item) {
				// add random behavior class
				$.each(behaviorArrValues, function (key, value) {
					$answer.removeClass('answer-behavior-' + value);
				});
				$answer.addClass('answer-behavior-' + behaviorArr.pop());


				// show answer & add text
        isSelected = ($.inArray(parseInt(key, 10), APP.questions[questionId].userAnswers) !== -1);
				$answer.show()
				  .css('display', 'table')
				  .data('value', key)
				  .toggleClass('selected', isSelected)
					.find('span').html(item);

				$answer = $answer.next();
			});
      $answer.prev().nextAll().hide();

			// put answers
      $.each(APP.questions[questionId].userAnswers, function (key, item) {
        initText(key + 1, APP.questions[questionId].variants[item]);
      });

      $game.attr('data-type', APP.questions[questionId].type);

			$('.js-question-answers').attr('data-state', APP.questions[questionId].userAnswers.length);
		};

		// next question
		/*$('.js-next').on('click', function () {
			// add points @todo

			$.ajax({
				dataType: 'json',
				url: 'ajax/1.json',
				data: APP.userAnswers[questionId],
				success: function () {
					questionId++;
					initQuestion();
					// add points @todo
				},
				error: function () {
					// add points or show error @todo
				}
			});
		});*/

    APP.initQuestion();
	});
}(jQuery));
